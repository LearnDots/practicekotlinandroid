package com.practicesession.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.practicesession.R
import com.practicesession.databinding.ActivityLoginBinding
import com.practicesession.register.RegisterActivity
import com.practicesession.viewModel.LoginViewModel

class LoginActivity : AppCompatActivity() {
    private lateinit var loginViewModel: LoginViewModel
    lateinit var mBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        mBinding.lifecycleOwner = this

        mBinding.loginViewModel = loginViewModel

        showData(loginViewModel)

    }

    private fun showData(loginViewModel: LoginViewModel) {
        loginViewModel.getUser().observe(this, Observer { userData ->
            if (TextUtils.isEmpty(userData.email)|| userData.email==null) {
                mBinding.txtEmailAddress.error = getString(R.string.input_email)
                mBinding.txtEmailAddress.requestFocus()
            } else if (!userData.isEmailValid()) {
                mBinding.txtEmailAddress.error = getString(R.string.input_valid_email)
                mBinding.txtEmailAddress.requestFocus()
            } else if (TextUtils.isEmpty(userData.password)|| userData.password==null) {
                mBinding.txtPassword.error = getString(R.string.input_password)
                mBinding.txtPassword.requestFocus()
            } else if (!userData.isPasswordLengthGreaterThan5()) {
                mBinding.txtPassword.error = getString(R.string.input_valid_password)
                mBinding.txtPassword.requestFocus()
            } else {
                mBinding.txtPassword.error = null
                mBinding.txtEmailAddress.error = null
            }
        })
    }

    fun onCreateAccount(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java));
    }
}


