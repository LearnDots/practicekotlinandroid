package com.practicesession.model

import android.util.Patterns

data class User(var email: String, var password: String, var username: String?=null){

    fun isEmailValid(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    fun isPasswordLengthGreaterThan5(): Boolean {
        return password.length > 5
    }
}