package com.practicesession.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.practicesession.R
import com.practicesession.databinding.ActivityLoginBinding
import com.practicesession.databinding.ActivityRegisterBinding
import com.practicesession.viewModel.LoginViewModel
import com.practicesession.viewModel.RegisterViewModel

class RegisterActivity : AppCompatActivity() {
    private lateinit var registerViewModel: RegisterViewModel
    lateinit var mBinding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        mBinding.lifecycleOwner = this
        mBinding.registerViewModel = registerViewModel

        showData(registerViewModel)

    }


    private fun showData(registerViewModel: RegisterViewModel) {
        registerViewModel.getUser().observe(this, Observer { userData ->
            if (TextUtils.isEmpty(userData.username)) {
                mBinding.txtUserName.error = getString(R.string.input_user_name)
                mBinding.txtUserName.requestFocus()
            }
            else if (TextUtils.isEmpty(userData.email) || userData.email==null) {
                mBinding.txtEmailAddress.error = getString(R.string.input_email)
                mBinding.txtEmailAddress.requestFocus()
            } else if (!userData.isEmailValid()) {
                mBinding.txtEmailAddress.error = getString(R.string.input_valid_email)
                mBinding.txtEmailAddress.requestFocus()
            } else if (TextUtils.isEmpty(userData.password)|| userData.password==null) {
                mBinding.txtPassword.error = getString(R.string.input_password)
                mBinding.txtPassword.requestFocus()
            } else if (!userData.isPasswordLengthGreaterThan5()) {
                mBinding.txtPassword.error = getString(R.string.input_valid_password)
                mBinding.txtPassword.requestFocus()
            } else {

                mBinding.txtPassword.error = null
                mBinding.txtEmailAddress.error = null
            }
        })
    }
}


