package com.practicesession.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.practicesession.model.User

class RegisterViewModel : ViewModel() {

    init {
       Log.i("LoginViewModel","LoginViewModel Created!!")
    }

    var emailAddress: MutableLiveData<String> = MutableLiveData()
    var userName: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()

    private var userMutableLiveData: MutableLiveData<User>? = null

    fun getUser(): MutableLiveData<User> {

        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData as MutableLiveData<User>
    }

    fun onClick() {
        val user = User(emailAddress.value.toString(), password.value.toString(), userName.value)
        userMutableLiveData?.value = user
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("LoginViewModel","LoginViewModel Destroyed!!")
    }

}